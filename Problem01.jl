### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ f9a1eb6d-37ba-49fb-8956-ee373ecf44af
using Markdown

# ╔═╡ d1d08a6d-03bd-4480-ba27-9e1d66f294b3
using InteractiveUtils

# ╔═╡ af09d856-4551-4aa0-888c-5c61ae19cb1c
using PlutoUI

# ╔═╡ 47c23630-8778-49c3-bfa2-ca85628056ac
using DataStructures

# ╔═╡ 52410346-e322-4f46-b924-ca34e8b79c8a
md"# Problem01"

# ╔═╡ 72dcb258-d3bc-41b9-922b-7c1ba793754d
md"#### State definition"

# ╔═╡ 4a749967-aae3-420f-9170-c93b3cca2361
struct State
    name::String
    position::Int64
   parcel::Vector{Bool}
end

# ╔═╡ 82ef3bd2-df72-406c-b825-cd5c36009067
md"#### Action defintion"

# ╔═╡ b602d5bd-1928-493a-8618-6cf0abc0141a
struct Action
    name::String
    cost::Int64
end

# ╔═╡ 79beb819-f32a-449f-ade9-2b2e87e07924
md"#### creating actions "

# ╔═╡ aa43a1d3-1b9a-414d-8417-ed8ccdb9cc2e
moveEast = Action("me", 2)

# ╔═╡ 4ed91a53-0ffc-4c07-b6cc-56c865efee23
moveWest = Action("mw", 2)

# ╔═╡ 742f9c6f-7f79-4d57-afa8-69b1e5c0d383
moveUp = Action("mu", 1)

# ╔═╡ ff818945-4b52-4ed5-a34b-e7eca53552b7
moveDown = Action("md", 1)

# ╔═╡ 11f45433-a886-4c7a-aa65-0f58f9e740bc
collect = Action("co", 5)

# ╔═╡ d12a35dc-db4f-4d53-9527-16f7f1c9ef00
md"###Creating the states "

# ╔═╡ e95bcf67-5be2-476c-930e-f21c9e0b8961
S1 = State("State A", 1, [true, false, true])

# ╔═╡ db9ca5e4-3b2c-4231-bbc2-50e6c3d7e841
S2 = State("State B", 2, [true, true, false])

# ╔═╡ 4f230053-10d2-427c-914d-d3ea8c7f5bc4
S3 = State("State C", 3, [false, true, true])

# ╔═╡ 1097cb8b-5213-414d-9993-605ebb4f4d82
S4 = State("State D", 4, [false, true, true])

# ╔═╡ d1dcc297-7fe1-493d-95ce-81963df4204f
S5 = State("State E", 5, [true, false, true])

# ╔═╡ 83f46cd6-94f4-44fb-a850-68442af98af9
S6 = State("State F", 1, [false, true, true])

# ╔═╡ 6c1367ea-1767-4e31-8ca6-3d51997a72e6
S7 = State("State G", 2, [false, false, true])

# ╔═╡ 86ab852b-4529-48b3-ab50-cea1b92ff71b
S8 = State("State H", 3, [false, false,true])

# ╔═╡ a7911dfc-7bf4-4e73-b396-fbd4a4143a2a
S9 = State("State I", 4, [false, false, true])

# ╔═╡ c8f60d1c-2fe6-419b-852a-3f91c2c0af4d
S10 = State("State K", 5, [false, false, false])

# ╔═╡ 181a4598-5041-4cb4-9a92-5c1308862230
md"#### Creating transition model"

# ╔═╡ abd0a4e4-6fb5-44c9-a928-50dc287f42ca
OfficeModel = Dict(S1 => [(moveWest,S2),(collect,S6),(moveUp,S1)], S1 =>[(moveWest,S2), (collect,S7),(moveEast,S2)], S1 => [(moveDown,S4),(collect,S8), (moveEast,S3)], S1 => [(moveWest,S5), (collect,S8), (moveUp,S4)], S1 => [(moveWest,S5),(collect,S9),(moveUp,S4)], S10 => [(moveWest,S5),(collect,S10),(moveDown,S10)])

# ╔═╡ 51f8e662-f695-4883-8992-c895311bcc5a
OfficeModel

# ╔═╡ 6ea4cd1d-2871-45fd-af74-f77bade3e5cf
md"#### User input"

# ╔═╡ a6056c1e-454e-48e5-a1bb-32f4c593cd38
@bind storeys TextField()

# ╔═╡ c61528b5-cd5e-4471-87d3-4105f6ccfbaf
with_terminal() do
	println( storeys)
end

# ╔═╡ 4ad87b09-fae3-4422-a1d9-ae10581f2c60
@bind offices TextField()

# ╔═╡ 3ccea985-d1c4-4a27-8abd-befa10d2a6d6
with_terminal() do
	println( offices)
end

# ╔═╡ 4f95052f-b69d-407c-b8eb-2744c5c8c26e
@bind parcels TextField()

# ╔═╡ 526e718a-ac9b-4b9c-8727-8a60f79837e8
with_terminal() do
	println(parcels)
end

# ╔═╡ 8b2973d7-04d7-4811-af21-0da3c68e544a
@bind location TextField()

# ╔═╡ 18cc00fc-356e-41d1-b404-41c380df6fa8
with_terminal() do
	println( location)
end

# ╔═╡ e3458cd5-9241-4aba-91c9-5e2b5171278f
md"#### Setting Initial State"

# ╔═╡ 72d3be1e-f320-4af0-b94e-dec4bb9bf7de
f(State=0) = ()->State+=1

# ╔═╡ ef15989b-8cae-4fdf-ac71-57ae9308ac78
foo = f()

# ╔═╡ 6f8f630d-07c5-409a-8517-c4983d8022e4
foo.State

# ╔═╡ 4f62b3cc-5bd5-403d-8bb4-11e697086363
foo.State.contents

# ╔═╡ 8530f9a9-7ef7-430e-84d5-1a016c9abeaa
md"#### Setting Goal State"

# ╔═╡ 0ec11f7d-9299-4156-acd3-d31b17430d9d
with_terminal() do
	println( S10)
end

# ╔═╡ 464b150d-65ad-4c88-ad57-e2a5b79a627f
md"#### Exploring the  A star Search "

# ╔═╡ 388f9a6c-eedc-4f4d-b43a-11a89a83d84f
function designAstar(State1, goalState)
    return dist(State1[1] - goalState[1]) + dist(State1[2] - goalState[2])
end

# ╔═╡ dc51f517-026a-4749-9204-caefdcea896d
function AStar_Strategy(OfficeModel,initialState, goalState)
	
    result = []
	frontier = Queue{State}()
	visited = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initialState)
	parent = initialState
	
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(visited, currentState)
			candidates = OfficeModel[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in visited)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return get_result(OfficeModel, parents,initialState, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ c2f3fe36-4c55-4222-9b23-deefa172ce53
function getResult(OfficeModel, ancestors, initialState, goalState)
	result = []
	visitor = goalState
	while !(visitor == initialState)
		current_state_ancestor = ancestors[visitor]
		related_transitions = OfficeModel[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == visitor
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		visitor = current_state_ancestor
	end
	return result
end

# ╔═╡ 1e973eb1-c7c3-4044-b215-4f31ce74e1e4
function searchPath(initialState, transition_dict, is_goal,all_candidates,
add_candidate, remove_candidate)
	visited = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initialState, 0)
	parent = initialState
	while true
		if isempty(the_candidates)
			return []
		else
			(s1, s2) = remove_candidate(the_candidates)
			current_state = s1
			the_candidates = s2
			push!(visited, current_state)
			candidates = transition_dict[current_state]
			for single_candidate in candidates
				if !(single_candidate[2] in visited)
					push!(ancestors, single_candidate[2] => current_state)
					if (is_goal(single_candidate[2]))
						return getResult(transition_dict, ancestors,
initialState, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates,
single_candidate[2], single_candidate[1].cost)
						end
					end
				end
			end
		end
end

# ╔═╡ 004f99e3-54d1-464b-ba72-da81353525c9
function goalTest(currentState::State)
    return ! (currentState.parcel[1] || currentState.parcel[2])
end

# ╔═╡ cdef6c96-0cfe-43a1-8097-1bda75ffcd1c
md"## Creating Agent's Operations"

# ╔═╡ c9146d55-0ffa-4be0-b918-95751abd473f
function addToQueue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ 85c20060-1aa5-43b1-a6d7-bf970b9412be
function addToStack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ b14c108d-ee5b-4066-9e23-ea0b46226b0e
function removeFromQueue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ 73082f5b-095c-415b-944d-211b976b734a
function removeFromStack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ a8da376a-fda0-4061-bb14-03d7a86d2bbd
md"##### A Star Search Strategy"

# ╔═╡ 2a2e298b-cd1a-497e-bd98-1cdf39f17a4d
searchPath(S1, OfficeModel, goalTest, Queue{State}(), addToQueue, removeFromQueue)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═52410346-e322-4f46-b924-ca34e8b79c8a
# ╠═f9a1eb6d-37ba-49fb-8956-ee373ecf44af
# ╠═d1d08a6d-03bd-4480-ba27-9e1d66f294b3
# ╠═af09d856-4551-4aa0-888c-5c61ae19cb1c
# ╠═47c23630-8778-49c3-bfa2-ca85628056ac
# ╠═72dcb258-d3bc-41b9-922b-7c1ba793754d
# ╠═4a749967-aae3-420f-9170-c93b3cca2361
# ╠═82ef3bd2-df72-406c-b825-cd5c36009067
# ╠═b602d5bd-1928-493a-8618-6cf0abc0141a
# ╠═79beb819-f32a-449f-ade9-2b2e87e07924
# ╠═aa43a1d3-1b9a-414d-8417-ed8ccdb9cc2e
# ╠═4ed91a53-0ffc-4c07-b6cc-56c865efee23
# ╠═742f9c6f-7f79-4d57-afa8-69b1e5c0d383
# ╠═ff818945-4b52-4ed5-a34b-e7eca53552b7
# ╠═11f45433-a886-4c7a-aa65-0f58f9e740bc
# ╠═d12a35dc-db4f-4d53-9527-16f7f1c9ef00
# ╠═e95bcf67-5be2-476c-930e-f21c9e0b8961
# ╠═db9ca5e4-3b2c-4231-bbc2-50e6c3d7e841
# ╠═4f230053-10d2-427c-914d-d3ea8c7f5bc4
# ╠═1097cb8b-5213-414d-9993-605ebb4f4d82
# ╠═d1dcc297-7fe1-493d-95ce-81963df4204f
# ╠═83f46cd6-94f4-44fb-a850-68442af98af9
# ╠═6c1367ea-1767-4e31-8ca6-3d51997a72e6
# ╠═86ab852b-4529-48b3-ab50-cea1b92ff71b
# ╠═a7911dfc-7bf4-4e73-b396-fbd4a4143a2a
# ╠═c8f60d1c-2fe6-419b-852a-3f91c2c0af4d
# ╠═181a4598-5041-4cb4-9a92-5c1308862230
# ╠═abd0a4e4-6fb5-44c9-a928-50dc287f42ca
# ╠═51f8e662-f695-4883-8992-c895311bcc5a
# ╠═6ea4cd1d-2871-45fd-af74-f77bade3e5cf
# ╠═a6056c1e-454e-48e5-a1bb-32f4c593cd38
# ╠═c61528b5-cd5e-4471-87d3-4105f6ccfbaf
# ╠═4ad87b09-fae3-4422-a1d9-ae10581f2c60
# ╠═3ccea985-d1c4-4a27-8abd-befa10d2a6d6
# ╠═4f95052f-b69d-407c-b8eb-2744c5c8c26e
# ╠═526e718a-ac9b-4b9c-8727-8a60f79837e8
# ╠═8b2973d7-04d7-4811-af21-0da3c68e544a
# ╠═18cc00fc-356e-41d1-b404-41c380df6fa8
# ╠═e3458cd5-9241-4aba-91c9-5e2b5171278f
# ╠═72d3be1e-f320-4af0-b94e-dec4bb9bf7de
# ╠═ef15989b-8cae-4fdf-ac71-57ae9308ac78
# ╠═6f8f630d-07c5-409a-8517-c4983d8022e4
# ╠═4f62b3cc-5bd5-403d-8bb4-11e697086363
# ╠═8530f9a9-7ef7-430e-84d5-1a016c9abeaa
# ╠═0ec11f7d-9299-4156-acd3-d31b17430d9d
# ╠═464b150d-65ad-4c88-ad57-e2a5b79a627f
# ╠═388f9a6c-eedc-4f4d-b43a-11a89a83d84f
# ╠═dc51f517-026a-4749-9204-caefdcea896d
# ╠═c2f3fe36-4c55-4222-9b23-deefa172ce53
# ╠═1e973eb1-c7c3-4044-b215-4f31ce74e1e4
# ╠═004f99e3-54d1-464b-ba72-da81353525c9
# ╠═cdef6c96-0cfe-43a1-8097-1bda75ffcd1c
# ╠═c9146d55-0ffa-4be0-b918-95751abd473f
# ╠═85c20060-1aa5-43b1-a6d7-bf970b9412be
# ╠═b14c108d-ee5b-4066-9e23-ea0b46226b0e
# ╠═73082f5b-095c-415b-944d-211b976b734a
# ╠═a8da376a-fda0-4061-bb14-03d7a86d2bbd
# ╠═2a2e298b-cd1a-497e-bd98-1cdf39f17a4d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
