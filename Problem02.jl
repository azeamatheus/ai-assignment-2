### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 63555ce7-90b1-4b45-bd61-5a3c9a0a232b
begin
using Markdown
using InteractiveUtils
using Printf
using DataStructures
using PlutoUI
end

# ╔═╡ 3d6698c0-cc1b-11ec-35b0-fd8f8f620cca
md"# Problem 2"

# ╔═╡ d672b572-7b63-4c07-bd84-7f5000fc87ba
md"#### Importing modules"

# ╔═╡ 5276fe94-b184-48dd-84f9-c113f5a41aa1
md"#### Creating neccessary structs"

# ╔═╡ b0403b92-58a1-4e83-80ff-cec3e828c46e
struct MyDomain
	a
	b
	c
	d
end

# ╔═╡ 197f73ee-01bb-48d8-9afc-87ae189a5f0a
abstract type AbstractCSP
end

# ╔═╡ ce57b4fe-e9a1-4b16-a571-fa25e8faef79
struct ConstantFunctionDict{V}
    value::V
end

# ╔═╡ 1db18879-cf61-4aba-ab71-df31928de6c4
mutable struct CSPDict
    dict::Union{Nothing, Dict, ConstantFunctionDict}
end

# ╔═╡ d53ba747-4baa-49b3-926b-c439c2d6f18c
mutable struct CSP <: AbstractCSP
	vars::AbstractVector
	domains::CSPDict
	neighbors::CSPDict
	constraints::Function
	initial::Tuple
	current_domains::Union{Nothing, Dict}
	nassigns::Int64
end

# ╔═╡ b6c4f7e7-ee9e-488f-b2bb-d6c0ea28565f
mutable struct CSPVar
	name::String
	value::Union{Nothing,MyDomain}
	forbidden_values::Vector{MyDomain}
	domain_restriction_count::Int64
end

# ╔═╡ d20a77e6-90b0-4851-95b3-158b3edb7893
struct Concrete
	vars::Vector{CSPVar}
	constraints::Vector{Tuple{CSPVar,CSPVar}}
end

# ╔═╡ be22f360-6fe9-4c10-b0ab-7d2c10e696df
struct Arc_graph{T <: Real,U}
    edges::Dict{Tuple{U,U},T}
    verts::Set{U}
end

# ╔═╡ 51f2155f-fd72-44b5-9f5a-dcce1cb8c8aa
md"#### Creating methods for the structs"

# ╔═╡ 9fcca43d-74dc-4606-8579-456b6a9524ca
function Cloud(vars::AbstractVector, domains::CSPDict, neighbors::CSPDict, constraints::Function;
                initial::Tuple=(), current_domains::Union{Nothing, Dict}=nothing, nassigns::Int64=0)
        return new(vars, MyDomain, neighbors, constraints, initial, current_domains, nassigns)
    end

# ╔═╡ b4d82ff6-148b-4fd9-80af-c64f34db1552
function Graph(edges::Vector{Tuple{U,U,T}}) where {T <: Real,U}
    vnames = Set{U}(v for edge in edges for v in edge[1:2])
    adjmat = Dict((edge[1], edge[2]) => edge[3] for edge in edges)
    return Arc_graph(adjmat, vnames)
end

# ╔═╡ ea420a01-c9f6-4d0d-806d-143667796047
begin
	vertices(g::Arc_graph) = g.verts
	edges(g::Arc_graph)    = g.edges
end

# ╔═╡ 311bd6f5-45d2-4651-a7fa-3f7ae5b85cae
#Set Signature Difference#

# ╔═╡ d6381fd5-bd3d-4587-b3d9-e765d0a3f369
signature = rand(setdiff(Set([a,b,c,d]), Set([b,c])))

# ╔═╡ b1662c26-4966-4013-8619-e0d9a43f4a74
neighbours(g::Arc_graph, v) = Set((b, c) for ((a, b), c) in edges(g) if a == v)

# ╔═╡ 9fffafc0-9688-4e8d-b6b8-57629179a2a2
function assign(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    assignment[key] = val;
    problem.nassigns = problem.nassigns + 1;
    nothing;
end

# ╔═╡ 732472a6-1819-49aa-981b-e4af20535796
#Unassign(problem, key, val, assignment)#

# ╔═╡ 3ce597a5-c972-49ea-acf4-52c0cddd08f6
function unassign(problem::T, key, assignment::Dict) where {T <: AbstractCSP}
    if (haskey(assignment, key))
        delete!(assignment, key);
    end
    nothing;
end

# ╔═╡ e97d56c1-f116-41ff-a7f9-894284dad260
#Number of conflicts#

# ╔═╡ 9465613f-c2a7-4870-9313-1d10baabd833
function nconflicts(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    return count(
                (function(second_key)
                    return (haskey(assignment, second_key) &&
                        !(problem.constraints(key, val, second_key, assignment[second_key])));
                end),
                problem.neighbors[key]);
end

# ╔═╡ 2674b1e2-079b-442c-8c8f-4b93495f35e3
function display(problem::T, assignment::Dict) where {T <: AbstractCSP}
    println("Cloud: ", problem, " with assignment: ", assignment);
    nothing;
end

# ╔═╡ 5e344665-6ea0-44b5-9c98-c38dca7e3f44
#Setting related Actions#

# ╔═╡ 8f0bf278-bd3a-4c9d-860b-4faa3b5babaa
function actions(problem::T, state::Tuple) where {T <: AbstractCSP}
    if (length(state) == length(problem.vars))
        return [];
    else
        let
            local assignment = Dict(state);
            local var = problem.vars[findfirst((function(e)
                                        return !haskey(assignment, e);
                                    end), problem.vars)];
            return collect((var, val) for val in problem.MyDomain[var]
                            if nconflicts(problem, var, val, assignment) == 0);
        end
    end
end

# ╔═╡ 25327681-471f-4c75-b5ce-45d394dca80a
function get_result(problem::T, state::Tuple, action::Tuple) where {T <: AbstractCSP}
    return (state..., action);
end

# ╔═╡ 20aa5bba-07b4-4859-97a5-908993607c52
#Search Strategy

# ╔═╡ 34e9726a-be64-459b-925d-1034ef17daa9
function search_path(g::Arc_graph{T,U}, source::U, dest::U) where {T, U}
    @assert source ∈ vertices(g) "$source is not a vertex in the graph"
 
    if source == dest return [source], 0 end
    
    inf  = typemax(T)
    dist = Dict(v => inf for v in vertices(g))
    prev = Dict(v => v   for v in vertices(g))
    dist[source] = 0
    Q = copy(vertices(g))
    neigh = Dict(v => neighbours(g, v) for v in vertices(g))
 
    
    while !isempty(Q)
        u = reduce((x, y) -> dist[x] < dist[y] ? x : y, Q)
        pop!(Q, u)
        if dist[u] == inf || u == dest break end
        for (v, cost) in neigh[u]
            alt = dist[u] + cost
            if alt < dist[v]
                dist[v] = alt
                prev[v] = u
            end
        end
    end
 
    rst, cost = U[], dist[dest]
    if prev[dest] == dest
        return rst, cost
    else
        while dest != source
            pushfirst!(rst, dest)
            dest = prev[dest]
        end
        pushfirst!(rst, dest)
        return rst, cost
    end
end

# ╔═╡ 39f4e403-75b8-4d92-b6e5-aba84ef030f7
#Defining Goal Test#

# ╔═╡ a3bf792f-9ec7-41c9-a7f7-18f0a2260103
function goal_test(problem::T, state::Dict) where {T <: AbstractCSP}
    let
        local assignment = deepcopy(state);
        return (length(assignment) == length(problem.vars) &&
                all((function(key)
                            return nconflicts(problem, key, assignment[key], assignment) == 0;
                        end),
                        problem.vars));
    end
end

# ╔═╡ 070b1ab6-41e2-48f5-9ff4-1f9ef5b2a470
function path_cost(problem::T, cost::Float64, state1::Tuple, action::Tuple, state2::Tuple) where {T <: AbstractCSP}
    return cost + 1;
end

# ╔═╡ d9f1ba66-016e-4c9b-b712-5fc8dc74cd18
function support_pruning(problem::T) where {T <: AbstractCSP}
    if (problem.current_domains === nothing)
        problem.current_domains = Dict(collect(Pair(key, collect(problem.domains[key])) for key in problem.vars));
    end
    nothing;
end

# ╔═╡ aed911f9-5d50-4010-a740-e975ce5a6369
function suppose(problem::T, key, val) where {T <: AbstractCSP}
    support_pruning(problem);
    local removals::AbstractVector = collect(Pair(key, a) for a in problem.current_domains[key]
                                            if (a != val));
    problem.current_domains[key] = [val];
    return removals;
end

# ╔═╡ f7afbdc8-c3dd-414b-b2ae-9e17797a8cdf
function pruning(problem::T, key, value, removals) where {T <: AbstractCSP}
    local not_removed::Bool = true;
    for (i, element) in enumerate(problem.current_domains[key])
        if (element == value)
            deleteat!(problem.current_domains[key], i);
            not_removed = false;
            break;
        end
    end
    if (not_removed)
        error("Could not find ", value, " in ", problem.current_domains[key], " for key '", key, "' to be removed!");
    end
    if (!(typeof(removals) <: Nothing))
        push!(removals, Pair(key, value));
    end
    nothing;
end

# ╔═╡ 8526bf4f-b3b3-4e39-bc34-95484a90b55a
function selection(problem::T, key) where {T <: AbstractCSP}
    if (!(problem.current_domains === nothing))
        return problem.current_domains[key];
    else
        return problem.MyDomain[key];
    end
end

# ╔═╡ f6bcc540-11d8-4585-a92a-414a110e2df5
function compare(problem::T) where {T <: AbstractCSP}
    support_pruning(problem);
    return Dict(collect(Pair(key, problem.current_domains[key][1])
                        for key in problem.vars
                            if (1 == length(problem.current_domains[key]))));
end

# ╔═╡ a56ada55-a7c4-4ba2-a063-2aa95277609f
function restore(problem::T, removals::AbstractVector) where {T <: AbstractCSP}
    for (key, val) in removals
        push!(problem.current_domains[key], val);
    end
    nothing;
end

# ╔═╡ 45f0e170-8d69-4bab-a9a0-a65b9e8defa8
function conflicted_variables(problem::T, current_assignment::Dict) where {T <: AbstractCSP}
    return collect(var for var in problem.vars
                    if (nconflicts(problem, var, current_assignment[var], current_assignment) > 0));
end

# ╔═╡ 7adff736-b449-458c-befd-459eb1f70393
#Forward Checking Strategy#

# ╔═╡ 9b619490-94d7-4f06-95ea-ad8f69990b09
function forward_checking(problem::T, var, value, assignment::Dict, removals::Union{Nothing, AbstractVector}) where {T <: AbstractCSP}
    for B in problem.neighbors[var]
        if (!haskey(assignment, B))
            for b in copy(problem.current_domains[B])
                if (!problem.constraints(var, value, B, b))
                    prune(problem, B, b, removals);
                end
            end
            if (length(problem.current_domains[B]) == 0)
                return false;
            end
        end
    end
    return true;
end

# ╔═╡ cb712551-27c6-4965-b6a6-667863cd1cd7
#Propagation Strategy#

# ╔═╡ ed7ff768-f7ef-463d-b606-89badc1e50e8
function backtrack(problem::T, assignment::Dict;
                    select_unassigned_variable::Function=first_unassigned_variable,
                    order_domain_values::Function=unordered_domain_values,
                    inference::Function=no_inference) where {T <: AbstractCSP}
    if (length(assignment) == length(problem.vars))
        return assignment;
    end
    local var = select_unassigned_variable(problem, assignment);
    for value in order_domain_values(problem, var, assignment)
        if (nconflicts(problem, var, value, assignment) == 0)
            assign(problem, var, value, assignment);
            removals = suppose(problem, var, value);
            if (inference(problem, var, value, assignment, removals))
                result = backtrack(problem, assignment,
                                    select_unassigned_variable=select_unassigned_variable,
                                    order_domain_values=order_domain_values,
                                    inference=inference);
                if (!(typeof(result) <: Nothing))
                    return result;
                end
            end
            restore(problem, removals);
        end
    end
    unassign(problem, var, assignment);
    return nothing;
end

# ╔═╡ 4133a857-3289-4979-9328-aee8b28dbcc6
testgraph = [("a", "b", 1), ("b", "c", 2), ("a", "d", 4)]

# ╔═╡ 3eee10a7-e15a-415f-92ae-3214a94c59ad
g = Graph(testgraph)

# ╔═╡ f6807f3b-7e38-4adb-938c-568f853b9be9
src, dst = "a", "d"

# ╔═╡ e83f4cba-fbd5-43ba-84ef-043fbc571c03
path, cost = search_path(g, src, dst)

# ╔═╡ 393c694f-43e6-49d7-8950-13a6890c8cab
with_terminal() do
	println("Cheapest path from $src to $dst: ", isempty(path) ? "no cheapest path" : join(path, " → "), " (cost $cost)")
end

# ╔═╡ d01ee221-3fb6-43e4-b442-7ae4a5efbbbf
with_terminal() do
	@printf("\n%3s | %2s | %s\n", "src", "dst", "path")
    @printf("_________________\n")
	for src in vertices(g), dst in vertices(g)
    path, cost = search_path(g, src, dst)
    @printf("%3s | %2s | %s\n", src, dst, isempty(path) ? "no cheapest path" : join(path, " → ") * " ($cost)")
	end
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Printf = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╟─3d6698c0-cc1b-11ec-35b0-fd8f8f620cca
# ╟─d672b572-7b63-4c07-bd84-7f5000fc87ba
# ╠═63555ce7-90b1-4b45-bd61-5a3c9a0a232b
# ╟─5276fe94-b184-48dd-84f9-c113f5a41aa1
# ╠═b0403b92-58a1-4e83-80ff-cec3e828c46e
# ╠═197f73ee-01bb-48d8-9afc-87ae189a5f0a
# ╠═ce57b4fe-e9a1-4b16-a571-fa25e8faef79
# ╠═1db18879-cf61-4aba-ab71-df31928de6c4
# ╠═d53ba747-4baa-49b3-926b-c439c2d6f18c
# ╠═b6c4f7e7-ee9e-488f-b2bb-d6c0ea28565f
# ╠═d20a77e6-90b0-4851-95b3-158b3edb7893
# ╠═be22f360-6fe9-4c10-b0ab-7d2c10e696df
# ╟─51f2155f-fd72-44b5-9f5a-dcce1cb8c8aa
# ╠═9fcca43d-74dc-4606-8579-456b6a9524ca
# ╠═b4d82ff6-148b-4fd9-80af-c64f34db1552
# ╠═ea420a01-c9f6-4d0d-806d-143667796047
# ╠═311bd6f5-45d2-4651-a7fa-3f7ae5b85cae
# ╠═d6381fd5-bd3d-4587-b3d9-e765d0a3f369
# ╠═b1662c26-4966-4013-8619-e0d9a43f4a74
# ╠═9fffafc0-9688-4e8d-b6b8-57629179a2a2
# ╠═732472a6-1819-49aa-981b-e4af20535796
# ╠═3ce597a5-c972-49ea-acf4-52c0cddd08f6
# ╠═e97d56c1-f116-41ff-a7f9-894284dad260
# ╠═9465613f-c2a7-4870-9313-1d10baabd833
# ╠═2674b1e2-079b-442c-8c8f-4b93495f35e3
# ╠═5e344665-6ea0-44b5-9c98-c38dca7e3f44
# ╠═8f0bf278-bd3a-4c9d-860b-4faa3b5babaa
# ╠═25327681-471f-4c75-b5ce-45d394dca80a
# ╠═20aa5bba-07b4-4859-97a5-908993607c52
# ╠═34e9726a-be64-459b-925d-1034ef17daa9
# ╠═39f4e403-75b8-4d92-b6e5-aba84ef030f7
# ╠═a3bf792f-9ec7-41c9-a7f7-18f0a2260103
# ╠═070b1ab6-41e2-48f5-9ff4-1f9ef5b2a470
# ╠═d9f1ba66-016e-4c9b-b712-5fc8dc74cd18
# ╠═aed911f9-5d50-4010-a740-e975ce5a6369
# ╠═f7afbdc8-c3dd-414b-b2ae-9e17797a8cdf
# ╠═8526bf4f-b3b3-4e39-bc34-95484a90b55a
# ╠═f6bcc540-11d8-4585-a92a-414a110e2df5
# ╠═a56ada55-a7c4-4ba2-a063-2aa95277609f
# ╠═45f0e170-8d69-4bab-a9a0-a65b9e8defa8
# ╠═7adff736-b449-458c-befd-459eb1f70393
# ╠═9b619490-94d7-4f06-95ea-ad8f69990b09
# ╠═cb712551-27c6-4965-b6a6-667863cd1cd7
# ╠═ed7ff768-f7ef-463d-b606-89badc1e50e8
# ╠═4133a857-3289-4979-9328-aee8b28dbcc6
# ╠═3eee10a7-e15a-415f-92ae-3214a94c59ad
# ╠═f6807f3b-7e38-4adb-938c-568f853b9be9
# ╠═e83f4cba-fbd5-43ba-84ef-043fbc571c03
# ╠═393c694f-43e6-49d7-8950-13a6890c8cab
# ╠═d01ee221-3fb6-43e4-b442-7ae4a5efbbbf
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
